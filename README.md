
```
#!python

# README #

  ________.______________ ________   ____ ___._____________
 /  _____/|   \__    ___/ \_____  \ |    |   \   \____    /
/   \  ___|   | |    |     /  / \  \|    |   /   | /     / 
\    \_\  \   | |    |    /   \_/.  \    |  /|   |/     /_ 
 \______  /___| |____|    \_____\ \_/______/ |___/_______ \
        \/                       \__>                    \/

Here is your quiz! I can't grade it unless you get it back to me via a pull request! 

1) Fork this repo to your own account in bitbucket
2) Clone your forked repo into your own cloud9 environment
3) Create a .php file with the name firstname_lastname.php (use your actual first and last name) and save the file
4) Add and Commit the change
5) Add a header with a h1 tag containing your name and a paragraph <p> about yourself in firstname_lastname.php and save the file
6) Add and Commit the change 
7) Create a separate branch (call it pizza) and check it out. 
8) Write a paragraph about your favorite pizza to the firstname_lastname.php file and save the file
9) Commit the change on the new pizza branch. 
10) Check out the master branch (you can only do this if you don't have any unsaved (committed) changes in the pizza branch)
11) Add a paragraph about your favorite place to go for vacation within firstname_lastname.php after the paragraph about pizza and save the file
12) Commit the change to the master branch
13) Merge the pizza branch into the master branch
14) Push your changes back to your forked repo
15) Create a pull request with your forked branch as the source branch and this repo as the destination. 
    Put your first and last name as the "Title" and write about your favorite GIT feature in the "Description"
    
  _____               ______   _       __ ______   ___   ___  __   __  
 |  __ \             |  ____| | |     /_ |____  | |__ \ / _ \/_ | / /  
 | |  | |_   _  ___  | |__ ___| |__    | |   / /     ) | | | || |/ /_  
 | |  | | | | |/ _ \ |  __/ _ \ '_ \   | |  / /     / /| | | || | '_ \ 
 | |__| | |_| |  __/ | | |  __/ |_) |  | | / /     / /_| |_| || | (_) |
 |_____/ \__,_|\___| |_|  \___|_.__/   |_|/_( )   |____|\___/ |_|\___/  at midnight!  
                                            |/                         
HINTS
use git status to see uncommited changes
and git log to see your history
```